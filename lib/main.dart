import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/route_manager.dart';
import 'package:new_food/routes/app_router.dart';

import 'theme/theme_constants.dart';
import 'theme/theme_manager.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static final ThemeManager _themeManager = ThemeManager();

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) => GetMaterialApp(
        title: 'New Food',
        debugShowCheckedModeBanner: false,
        getPages: AppRouterPage.appPages,
        initialRoute: AppRouterPage.initial,
        unknownRoute: AppRouterPage.notFound,
        theme: lightTheme,
        darkTheme: darkTheme,
        themeMode: _themeManager.themeMode,
      ),
    );
  }
}
