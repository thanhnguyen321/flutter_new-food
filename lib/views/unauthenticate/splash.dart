import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:new_food/gen/assets.gen.dart';
import 'package:new_food/gen/colors.gen.dart';
import 'package:new_food/routes/app_path.dart';
import 'package:new_food/share/constants/key.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  _getPage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString(Constants.tokken) ?? '';
    if (token == '') {
      Get.toNamed(RoutesPath.login);
    } else {
      Get.toNamed(RoutesPath.home);
    }
  }

  @override
  void initState() {
    _getPage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(AssetsImage.images.logo.path),
      ),
    );
  }
}
