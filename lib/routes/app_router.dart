import 'package:get/route_manager.dart';
import 'package:new_food/routes/app_path.dart';
import 'package:new_food/views/root_view.dart';

class AppRouterPage {
  static String initial = RoutesPath.splash;
  static final notFound =
      GetPage(name: RoutesPath.splash, page: () => const Splash());

  static List<GetPage> unAuthPages = [
    GetPage(
      name: RoutesPath.home,
      page: () => const HomePage(),
    ),
    GetPage(
      name: RoutesPath.cart,
      page: () => const CartPage(),
    ),
  ];
  static List<GetPage> authPages = [
    GetPage(
      name: RoutesPath.splash,
      page: () => const Splash(),
    ),
    GetPage(
      name: RoutesPath.login,
      page: () => const Login(),
    ),
    GetPage(
      name: RoutesPath.register,
      page: () => const Register(),
    ),
  ];
  static List<GetPage> appPages = [...authPages, ...unAuthPages];
}
