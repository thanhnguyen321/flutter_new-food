class RoutesPath {
  static String splash = '/splash';
  static String login = '/login';
  static String register = '/register';

  static String home = '/home';
  static String cart = '/cart';
}
